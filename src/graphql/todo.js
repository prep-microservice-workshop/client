import {gql} from '@apollo/client'

const CREATE_TODO = gql`
    mutation createTodo(
        $title: String!
    ) {
        todo{
            create(new_todo: $title) {
                id
                text
                is_done
                user{
                    id
                    username
                }
            }
        }
    }
`

const UPDATE_TODO_STATUS = gql`
    mutation updateTodoStatus(
        $id: Int!
        $is_done: Boolean!
    ) {
        todo {
            update_status(id: $id, is_done: $is_done) {
                id
                text
                is_done
                user{
                    id
                    username
                }
            }
        }
    }
`

const DELETE_TODO = gql`
    mutation deleteTodo(
        $id: Int!
    ) {
        todo{
            delete(id: $id)
        }
    }
`

export {
    CREATE_TODO,
    UPDATE_TODO_STATUS,
    DELETE_TODO
}