import {gql} from '@apollo/client'

const REGISTER = gql`
    mutation register(
        $username: String!
        $password: String!
    ) {
        register(input: {
            password: $password
            username: $username
        })
    }
`

const LOGIN = gql`
    mutation login(
        $username: String!
        $password: String!
    ) {
        login(input: {
            password: $password
            username: $username
        })
    }
`

const ME = gql`
    query me {
        me{
            hashed_password
            id
            username
            todos{
                id
                text
                is_done
            }
        }
    }
`

export {
    REGISTER,
    LOGIN, ME
}