
import { useMutation } from '@apollo/client';
import { Button, TextField } from '@mui/material';
import React from 'react';
import { ME } from '../../graphql/account';
import { CREATE_TODO } from '../../graphql/todo';
import { useForm } from '../../hook/useForm';

//todo form with react hooks
export const TodoForm = () => {
    
    const [AddTodo] = useMutation(CREATE_TODO, {
        update(cache, { data: { todo: { create } } }) {

            const { me : { todos } } = cache.readQuery({ query: ME });
            cache.writeQuery({
                query: ME,
                data: { me: { todos: todos.concat([create]) }}
            });
        },
        onError(err) {
            alert(err.message);
        }
    })

    const addTodo = () => {

        if (validateForm()) {
            AddTodo({
                variables: {
                    title: values.text
                }
            })
    
            values.text = '';
        }      
    }

    const validateForm = () => {
        if (values.text.trim() === '') {
            return false;
        } 
        return true; 
    }

    const { onSubmit, onChange, values } = useForm(addTodo, {
        text: ''
    });

    return (
        <form onSubmit={onSubmit} style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center', 
            padding: '1rem',         
        }}>
            <TextField 
                name="text"
                label="New Todo Here" 
                onChange={onChange}
                value={values.text}
                variant="filled" 
                style={{margin: 10}}/>

            <Button type="submit" variant="contained" color="primary" >Add New Todo</Button>
        </form>
    );
}
