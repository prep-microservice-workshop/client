import React from 'react'
import { TodoForm } from './AddTodoForm'
import { Navigate } from 'react-router-dom'
import { useQuery } from '@apollo/client';
import { ME } from '../../graphql/account';
import { Button, List, ListSubheader, Paper } from '@mui/material';
import { TodoItem } from './components/TodoItem';


//create todolist component with react hook
export const TodoList = () => {

    const { data } = useQuery(ME)

    if(!localStorage.getItem('access_token')) {
        return <Navigate to="/"/>
    }

    return (
        <React.Fragment>
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center', 
                height: '100vh'            
            }}>
                <Button variant="contained" color="primary" onClick={() => {
                    localStorage.removeItem('access_token')
                    window.location.reload()
                }} >Log Out</Button>
                
                <Paper style={{height: "50%", width: '360px', overflow: 'auto', marginTop: 5}}>
                    <List dense sx={{ width: '100%', bgcolor: 'background.paper' }}
                        subheader={
                            <ListSubheader style={{textAlign: 'center'}}>
                                {`${data && data.me && data.me && data.me.username}'s Todo List`}
                            </ListSubheader>
                        }
                    >                   
                        {
                            data && data.me && data.me.todos && data.me.todos.map((todo, index) => {
                                return (
                                    <TodoItem todo={todo} index={index}/>
                                )
                            })
                        }
                    </List>
                </Paper>

                <TodoForm />
            </div>           
        </React.Fragment>      
    )
}
