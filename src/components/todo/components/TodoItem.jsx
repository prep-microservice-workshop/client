import React from 'react'
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import Checkbox from '@mui/material/Checkbox';
import { ListItemIcon } from '@mui/material';
import { DELETE_TODO, UPDATE_TODO_STATUS } from '../../../graphql/todo';
import { useMutation } from '@apollo/client';
import { ME } from '../../../graphql/account';


export const TodoItem = ({ todo,index }) => {

    const [UpdateTodoStatus] = useMutation(UPDATE_TODO_STATUS, {
        update(cache, { data: { todo : { update_status } } }) {

            const { me : { todos } } = cache.readQuery({ query: ME });
            cache.writeQuery({
                query: ME,
                data: { me: { todos: todos.map(t => t.id === update_status.id ? update_status : t) }}
            });

        },
        onError(err) {
            alert(err.message)
        }
    })

    const [DeleteTodo] = useMutation(DELETE_TODO, {
        update(cache, { data: { todo } }) {
                
            const { me : { todos } } = cache.readQuery({ query: ME });
            cache.writeQuery({
                query: ME,
                data: { me: { todos: todos.filter(t => t.id !== todo.delete )}}
            });  
        },
        onError(err) {
            alert(err.message)
        },
    })

    return (
        <ListItem 
            secondaryAction={
                <IconButton edge="end" onClick={() => {
                    DeleteTodo({ variables: { id: parseInt(todo.id) } })
                }}>
                    <DeleteIcon />
                </IconButton>
            }
            key={index} 
            component="div">

            <ListItemIcon>
                <Checkbox
                    onClick={() => {
                        UpdateTodoStatus({
                            variables: {
                                id: parseInt(todo.id),
                                is_done: !todo.is_done
                            }
                        })
                    }}
                    edge="start"
                    checked={todo.is_done}
                    tabIndex={-1}
                />
            </ListItemIcon>
                                            
            <ListItemText primary={`${todo.text}`} />                                      
        </ListItem>
    )    
}
