import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useForm } from '../../hook/useForm'
import { REGISTER, LOGIN } from '../../graphql/account';
import { useMutation } from '@apollo/client';
import CustomizedSnackbars from '../helper/SnackBar';
import CircularProgress from '@mui/material/CircularProgress';
import { Navigate } from 'react-router';

function Copyright(props) {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" href="https://mui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const theme = createTheme();

export default function SignInOrSignUp() {

    const [func, setFunc] = React.useState(() => {})
    const [snackBarState, setSnackBarState] = React.useState({
        open: false,
        message: '',
        severity: 'info',
        onClose: () => {
            setSnackBarState({ ...snackBarState, open: false })
        }
    })

    const { onSubmit, onChange, values } = useForm(func,{
        username: '',
        password: ''
    })

    const [Login, { loadingLogin }] = useMutation(LOGIN, {
        update(_, { data: { login } }) {
            localStorage.setItem('access_token', login)
        }, onError(err) {
            setSnackBarState({
                ...snackBarState,
                open: true,
                message: err.graphQLErrors[0].message,
                severity: 'error'
            })
        }
    });

    const [Register, { loadingRegister }] = useMutation(REGISTER, {
        update(_, { data: { register } }) {
            localStorage.setItem('access_token', register)
        }, onError(err) {
            setSnackBarState({
                ...snackBarState,
                open: true,
                message: err.graphQLErrors[0].message,
                severity: 'error'
            })
        }
    });

    const signIn = () => {
        if(validateForm()) {
            Login({
                variables: {
                    username: values.username,
                    password: values.password
                }
            })
        }
    }
    const signUp = () => {
        if(validateForm()){
            Register({
                variables: {
                    username: values.username,
                    password: values.password
                }
            })
        }    
    }

    const validateForm = () => {

        if (values.username.trim() === '') {
            setSnackBarState({
                ...snackBarState,
                open: true,
                message: 'Username is required!',
                severity: 'error'
            })

            return false
        }

        if (values.password.trim() === '') {
            setSnackBarState({
                ...snackBarState,
                open: true,
                message: 'Password is required!',
                severity: 'error'
            })

            return false
        }

        return true
    }

    if(localStorage.getItem('access_token')) {
        return <Navigate to="/todos"/>
    }
    
    return (
        <React.Fragment>
            <ThemeProvider theme={theme}>
                <Container component="main" maxWidth="xs">
                    <CssBaseline />
                    <Box
                        sx={{
                            marginTop: 8,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >

                        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                            <LockOutlinedIcon />
                        </Avatar>

                        <Typography component="h1" variant="h5">
                            Sign in Or Sign Up
                        </Typography>

                        <Box component="form" onSubmit={onSubmit} noValidate sx={{ mt: 1 }}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="email"
                                onChange={onChange}
                                value={values.username}
                                label="Username"
                                name="username"
                                autoFocus
                            />
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                onChange={onChange}
                                value={values.password}
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                            />
                            
                            <Grid container>
                                <Grid item xs style={{marginRight: 10}}>
                                    <Button
                                        fullWidth
                                        variant="contained"
                                        onClick={() => {
                                            setFunc(signIn)
                                        }}
                                        sx={{ mt: 3, mb: 2 }}
                                    >
                                        {loadingLogin ? <CircularProgress /> : 'Sign In'}
                                    </Button>
                                </Grid>

                                <Grid item xs>
                                    <Button
                                        fullWidth
                                        variant="contained"
                                        onClick={() => {
                                            setFunc(signUp)
                                        }}
                                        sx={{ mt: 3, mb: 2 }}
                                    >
                                        {loadingRegister ? <CircularProgress /> : 'Sign Up'}
                                    </Button>
                                </Grid>
                            </Grid>
                        </Box>
                    </Box>

                    <Copyright sx={{ mt: 8, mb: 4 }} />

                    <CustomizedSnackbars state={snackBarState}/>
                </Container>
            </ThemeProvider>
        </React.Fragment>
    );
}