import './App.css';
import { setContext } from 'apollo-link-context';
import { ApolloClient, InMemoryCache, createHttpLink, ApolloProvider } from '@apollo/client'
import { TodoList } from './components/todo/TodoList';
import {
	BrowserRouter as Router,
	Routes,
	Route,
  } from "react-router-dom";
import SignInOrSignUp from './components/account/SignInOrSignUp';

function createClient() {
	const authLink = setContext(() => {
		const token = localStorage.getItem('access_token')
		return {
			headers: {
				Authorization: token ? `Bearer ${token}` : ''
			}
		}
	})
	
	const httpLink = createHttpLink({
		uri: `${process.env.REACT_APP_API_ENDPOINT}`,
	})
	
	const client = new ApolloClient({
		link: authLink.concat(httpLink),
		cache: new InMemoryCache()
	})

	return client
}


function App() {


    return (
		<ApolloProvider client={createClient()}>
			<Router>
				<Routes>
					<Route path="/todos" element={<TodoList />} />					
					<Route path="/" element={<SignInOrSignUp />} />						
				</Routes>
			</Router>
		</ApolloProvider>		
    );
}

export default App;
